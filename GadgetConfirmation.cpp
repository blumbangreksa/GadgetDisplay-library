#include "GadgetConfirmation.h"

GadgetConfirmation::GadgetConfirmation(U8GLIB &u8g) : GadgetMenu(u8g, selection, 2) {
  this->u8g = &u8g;
  strcpy_P(this->confirmationText, (const char *) F("Are you sure?"));
  strcpy_P(this->selection[0], (const char *) F("Yes"));
  strcpy_P(this->selection[1], (const char *) F("No"));
  this->menu = selection;
  this->totalMenu = 2;
  this->position = 0;
  this->u8g->setFont(u8g_font_unifont);
  this->u8g->setColorIndex(1);
}

void GadgetConfirmation::drawMenu(void) {
  int x = 0, y = 12;
  for (size_t i = 0; (i * 16) < strlen(confirmationText) && i < 2; i++) {
    char buffer[17];
    strncpy(buffer, confirmationText + (i * 16), 16);
    buffer[16] = 0;
    u8g->drawStr(x, y + (16 * i), buffer);
  }

  x = 0;
  y = 12 + (16 * 2);
  for (size_t i = 0; i < 2; i++) {
    u8g->drawStr(x + 16, y + (16 * i), menu[i]);
  }
  for (unsigned char i = 0; i < 2; i++) {
    if (position == i) {
      u8g->drawStr(x, y + (16 * i), ">");
    }
  }
}

GadgetConfirmation &GadgetConfirmation::setConfirmationText(const char *confirmationText, const char *yesText, const char *noText) {
  strncpy(this->confirmationText, confirmationText, 32);
  this->confirmationText[32] = 0;
  strncpy(this->selection[0], yesText, 16);
  this->selection[0][16] = 0;
  strncpy(this->selection[1], noText, 16);
  this->selection[1][16] = 0;
  return *this;
}

bool GadgetConfirmation::getConfirmationPosition(void) {
  return getPosition() == 0;
}
