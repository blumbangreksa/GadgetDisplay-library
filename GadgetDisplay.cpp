#include "GadgetDisplay.h"

GadgetDisplay::GadgetDisplay(U8GLIB &u8g) {
  this->u8g = &u8g;
  this->u8g->setFont(u8g_font_unifont);
  this->u8g->setColorIndex(1);
}

GadgetDisplay &GadgetDisplay::setFont(const u8g_fntpgm_uint8_t *font) {
  this->u8g->setFont(font);
  return *this;
}

GadgetDisplay &GadgetDisplay::setTime(int hours, int minutes) {
  this->hours = (hours < 24) ? hours : 23;
  this->minutes = (minutes < 60) ? minutes : 59;
  return *this;
}

GadgetDisplay &GadgetDisplay::setOperator(const char operatorName[]) {
  int length = strlen(operatorName) + 1;
  if (length < 4) {
    length = 4;
  }
  char operatorNameBuffer[length];
  sprintf(operatorNameBuffer, "%3s", operatorName);
  strncpy(this->operatorName, operatorNameBuffer, 3);
  this->operatorName[length] = '\0';
  return *this;
}

GadgetDisplay &GadgetDisplay::setSignal(int strength) {
  if (strength > 5) strength = 5;
  this->strength = strength;
  return *this;
}

GadgetDisplay &GadgetDisplay::setBatt(int power) {
  this->power = power;
  return *this;
}

GadgetDisplay &GadgetDisplay::setGPRSState(int gprsState) {
  this->gprsState = gprsState;
  return *this;
}

GadgetDisplay &GadgetDisplay::setString(int row, int column, const char s[], ...) {
  char buffer[17];
  va_list args;
  va_start(args, s);
  vsnprintf(buffer, 17, s, args);
  va_end(args);
  strcpy(table[row][column], buffer);
  tableChanged[row][column] = true;
  return *this;
}

GadgetDisplay &GadgetDisplay::draw(bool showstatusbar) {
  u8g->firstPage();
  do {
    if (showstatusbar) {
      drawTime();
      drawOperator();
      drawSignal();
      drawBattery();
      switch (gprsState) {
        case 0:;
          break;
        case 1:
          drawG();
          break;
        case 2:
          drawG();
          drawGrectangle();
          break;
      }
    }
    drawString();
  } while (u8g->nextPage());
  for (size_t i = 0; i < 4; i++) {
    for (size_t j = 0; j < 2; j++) {
      tableChanged[i][j] = false;
    }
  }
  return *this;
}

void GadgetDisplay::drawGrectangle() {
  int x = 96, y = 0;
  u8g->drawHLine(x, y + 1, 8);
  u8g->drawHLine(x, y + 8, 4);
  u8g->drawVLine(x, y + 1, 8);
  u8g->drawVLine(x + 7, y + 1, 4);
}

void GadgetDisplay::drawG() {
  int x = 96, y = 0;
  u8g->drawHLine(x + 2, y + 3, 4);
  u8g->drawHLine(x + 2, y + 6, 4);
  u8g->drawHLine(x + 4, y + 5, 2);
  u8g->drawVLine(x + 2, y + 3, 4);
}

void GadgetDisplay::drawTime() {
  char buffer[6];
  int x = 0, y = 12;
  sprintf(buffer, "%02d:%02d", hours, minutes);
  u8g->drawStr(x, y, buffer);
}

void GadgetDisplay::drawOperator() {
  int x = 69, y = 12;
  u8g->drawStr(x, y, operatorName);
}

void GadgetDisplay::drawSignal() {
  int x = 94, y = 0;
  u8g->drawHLine(x + 2, y + 14, 14);
  u8g->drawVLine(x + 15, y + 1, 14);
  u8g->drawLine(x + 2, y + 14, x + 15, y + 1);
  u8g->drawTriangle(x + 2, y + 14, x + 2 + 2 * strength + 3, y + 14, x + 2 + 2 * strength + 3, 16 - (x + 2 + 2 * strength + 3) + x + 2 + y - 3);
}

void GadgetDisplay::drawBattery() {
  int x = 111, y = 0;
  int power = this->power / 10;
  u8g->drawHLine(x + 3, y + 14, 10);
  u8g->drawVLine(x + 3, y + 3, 11);
  u8g->drawVLine(x + 12, y + 3, 11);
  u8g->drawHLine(x + 3, y + 3, 2);
  u8g->drawHLine(x + 10, y + 3, 3);
  u8g->drawVLine(x + 5, y + 1, 3);
  u8g->drawVLine(x + 10, y + 1, 2);
  u8g->drawHLine(x + 6, y + 1, 5);
  if (power < 10 && power != 0) {
    u8g->drawBox(x + 4, y + 14 - power, 9, power);
  } else if (power == 10) {
    u8g->drawBox(x + 4, y + 14 - 10, 9, 10);
    u8g->drawBox(x + 5, y + 1, 6, 3);
  }
}

void GadgetDisplay::drawString() {
  int x = 0, y = 12;
  for (size_t i = 0; i < 4; i++) {
    for (size_t j = 0; j < 2; j++) {
      if (tableChanged[i][j] == true) {
        u8g->drawStr(x + (64 * j), y + (16 * i), table[i][j]);
      }
    }
  }
}

void GadgetDisplay::clear() {
  for (size_t i = 0; i < 4; i++) {
    for (size_t j = 0; j < 2; j++) {
      setString(i, j, "");
    }
  }
  draw(false);
}
