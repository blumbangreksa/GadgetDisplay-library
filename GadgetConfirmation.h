#ifndef GADGET_CONFIRMATION_H
#define GADGET_CONFIRMATION_H

#include <U8glib.h>
#include "GadgetMenu.h"

class GadgetConfirmation : public GadgetMenu {
 private:
  char confirmationText[33];
  char selection[2][17];

  void drawMenu(void);

 public:
  GadgetConfirmation(U8GLIB &u8g);
  GadgetConfirmation &setConfirmationText(const char *confirmationText, const char *yesText, const char *noText);

  bool getConfirmationPosition(void);
};

#endif
