#include "GadgetSplash.h"

GadgetSplash::GadgetSplash(U8GLIB &u8g) {
  this->u8g = &u8g;
  this->x = 0;
  this->y = 0;
  this->count = 0;
  this->height = 0;
  this->image = NULL;
  strcpy_P(this->info, "Info...");
  this->u8g->setFont(u8g_font_unifont);
  this->u8g->setColorIndex(1);
}

GadgetSplash &GadgetSplash::draw(void) {
  u8g->firstPage();
  do {
    drawImage();
    drawInfo();
  } while (u8g->nextPage());
  return *this;
}

GadgetSplash &GadgetSplash::setImage(unsigned int x, unsigned int y, unsigned int count, unsigned int height, const u8g_pgm_uint8_t *image) {
  this->x = x;
  this->y = y;
  this->count = count;
  this->height = height;
  this->image = image;

  return *this;
}

GadgetSplash &GadgetSplash::setInfo(const char *info) {
  strncpy(this->info, info, 16);
  this->info[16] = '\0';

  return *this;
}

void GadgetSplash::drawImage(void) {
  u8g->drawBitmapP(x, y, count, height, image);
}

void GadgetSplash::drawInfo(void) {
  int x = 0, y = 12;
  u8g->drawStr(x, y + (16 * 3), info);
}
