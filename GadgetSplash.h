#ifndef GADGET_SPLASH_H
#define GADGET_SPLASH_H

#include <U8glib.h>

class GadgetSplash {
 private:
  U8GLIB *u8g;
  unsigned int x, y;
  unsigned int count, height;
  const u8g_pgm_uint8_t *image;
  char info[17];

  void drawImage(void);
  void drawInfo(void);

 public:
  GadgetSplash(U8GLIB &u8g);
  GadgetSplash &setImage(unsigned int x, unsigned int y, unsigned int count, unsigned int height, const u8g_pgm_uint8_t *image);
  GadgetSplash &setInfo(const char *info);
  GadgetSplash &draw(void);
};

#endif
