#ifndef GadgetDisplay_H
#define GadgetDisplay_H

#include <U8glib.h>

class GadgetDisplay {
 private:
  U8GLIB *u8g;
  int hours, minutes;
  int strength, power, gprsState;
  char operatorName[4];
  char table[4][2][17];
  bool tableChanged[4][2];
  void drawG();
  void drawGrectangle();
  void drawTime();
  void drawOperator();
  void drawSignal();
  void drawBattery();
  void drawString();

 public:
  GadgetDisplay(U8GLIB &u8g);
  GadgetDisplay &setFont(const u8g_fntpgm_uint8_t *font);
  GadgetDisplay &setTime(int hours, int minutes);
  GadgetDisplay &setOperator(const char operatorName[]);
  GadgetDisplay &setSignal(int strength);
  GadgetDisplay &setBatt(int power);
  GadgetDisplay &setString(int row, int column, const char s[], ...);
  GadgetDisplay &draw(bool showstatusbar = true);
  GadgetDisplay &setGPRSState(int state);

  void clear();
};

#endif
