#ifndef GADGET_MENU_H
#define GADGET_MENU_H

#include <U8glib.h>

class GadgetMenu {
 protected:
  U8GLIB *u8g;
  char (*menu)[17];
  unsigned char totalMenu;
  unsigned char position;

  virtual void drawMenu(void);

 public:
  GadgetMenu(U8GLIB &u8g, char (*menu)[17], unsigned char totalMenu);
  GadgetMenu &setFont(const u8g_fntpgm_uint8_t *font);
  GadgetMenu &next(void);
  GadgetMenu &previous(void);
  GadgetMenu &resetPosition(void);
  GadgetMenu &draw(void);

  unsigned char getPosition(void);

  void clear();
};

#endif
