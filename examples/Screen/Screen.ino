#include <U8glib.h>
#include <GadgetDisplay.h>

U8GLIB_SSD1306_128X64_2X u8g;
GadgetDisplay display(u8g);
int hour, minute, GPRSstatus, signal, power, stringplacement=1;

void setup() {
  Serial.begin(9600);
  Serial.println("GadgetDisplay library example");
}

void loop() {
  display.setTime(hour, minute)
         .setSignal(signal)
         .setBatt(power)
         .setString(stringplacement,stringplacement,"hello")
         .setGPRSState(GPRSstatus)
         .draw();
  delay(5000);
  incrementvariable();
}

void incrementvariable(){
  stringplacement++;
  hour++;
  minute++;
  GPRSstatus++;
  signal++;
  power++;
  if(hour > 24) hour = 0;
  if(minute > 59) minute = 0;
  if(GPRSstatus > 3) GPRSstatus = 0;
  if(signal > 5) signal = 0;
  if(power > 8) power = 0;
  if(stringplacement > 2) stringplacement = 1;
}
