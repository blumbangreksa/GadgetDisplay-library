#include "GadgetMenu.h"

GadgetMenu::GadgetMenu(U8GLIB &u8g, char menu[][17], unsigned char totalMenu) {
  this->u8g = &u8g;
  this->menu = menu;
  this->totalMenu = totalMenu;
  this->position = 0;
  this->u8g->setFont(u8g_font_unifont);
  this->u8g->setColorIndex(1);
}

GadgetMenu &GadgetMenu::setFont(const u8g_fntpgm_uint8_t *font) {
  this->u8g->setFont(font);
  return *this;
}

GadgetMenu &GadgetMenu::next(void) {
  position++;
  position = position % totalMenu;
  return *this;
}

GadgetMenu &GadgetMenu::previous(void) {
  position += totalMenu;
  position--;
  position = position % totalMenu;
  return *this;
}

GadgetMenu &GadgetMenu::resetPosition(void) {
  position = 0;
  return *this;
}
GadgetMenu &GadgetMenu::draw(void) {
  u8g->firstPage();
  do {
    drawMenu();
  } while (u8g->nextPage());
  return *this;
}

void GadgetMenu::clear() {

}

void GadgetMenu::drawMenu(void) {
  int x = 0, y = 12;
  unsigned char totalMenuShowed = totalMenu < 4 ? totalMenu : 4;
  static unsigned char menuOffset = position < 4 ? 0 : position - totalMenuShowed - 1;
  char *menuShowed[totalMenuShowed];

  if (position < menuOffset) {
    menuOffset = position;
  } else if(menuOffset + totalMenuShowed - 1 < position) {
    menuOffset = position - totalMenuShowed + 1;
  }

  for (size_t i = 0; i < totalMenuShowed; i++) {
    menuShowed[i] = menu[i + menuOffset];
    u8g->drawStr(x + 16, y + (16 * i), menuShowed[i]);
  }
  for (unsigned char i = 0; i < totalMenuShowed; i++) {
    if (position - menuOffset == i) {
      u8g->drawStr(x, y + (16 * i), ">");
    }
  }
}

unsigned char GadgetMenu::getPosition(void) {
  return position;
}
